Module: Toggle Area Layout
Author: David Fröhlich


Description
===========
Adds a "Toggle Area" layout type to your website, that allows you to
make it's contents collapsible.


Installation
============
Copy the 'toggle_area_layout' module directory in to your Drupal 'modules'
directory as usual.


Usage
=====
When using the Layout Builder, select the "Toggle Area" layout type.
Next, use the "Layout Options" to set a title and whether the element should
be open or closed by default.
